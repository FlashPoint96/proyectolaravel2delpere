@extends('layouts.app')

@section('content')

    <h2>Nuevo Objeto</h2>

    <form method="post" action="/objetos" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group row">
            <label for="NombreObjetoId" class="col-sm-3 col-form-label">NombreObjeto</label>
            <div class="col-sm-9">
                <input name="nom" type="text" class="form-control" id="NombreObjetoId" placeholder="Nombre Objeto">
            </div>
        </div>
		        <div class="form-group row">
            <label for="DescripcionObjetoId" class="col-sm-3 col-form-label">DescripcionObjeto</label>
            <div class="col-sm-9">
                <input name="descripcion" type="text" class="form-control" id="DescripcionObjetoId" placeholder="Nombre Objeto">
            </div>
        </div>
        <div class="form-group row">
            <label for="LvLUseId" class="col-sm-3 col-form-label">Level for Use</label>
            <div class="col-sm-9">
                <input name="LvLUse" type="number" class="form-control" id="LvLUseId"
                       placeholder="LvLUse">
            </div>
        </div>
        <div class="form-group row">
            <label for="Categoria" class="col-sm-3 col-form-label">Categoria</label>
            <div class="col-sm-9">
                <select id="categoria" name="enumCategoria">
                  <option value="Armas">Armas</option>
                  <option value="Chaleco">Chaleco</option>
                  <option value="DF">DF</option>
                  <option value="Navajas">Navajas</option>
                  <option value="Etc">Etc</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="Foto" class="col-sm-3 col-form-label">Foto</label>
            <div class="col-sm-9">
                <label for="Foto">Choose Image</label>
                <input id="Foto" type="file" name="Foto">
            </div>
        </div>
        <br>
        <div class="form-group row">
            <div class="offset-sm-3 col-sm-9">
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </form>

@endsection
