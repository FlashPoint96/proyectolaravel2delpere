<!DOCTYPE html>
<html lang="es">
<head>
    <title>New Order</title>
    <script src="{{ mix('js/app.js') }}"></script>
    <link href="{{ asset('/css/nav.css') }}" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="{{ asset('/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('/js/jquery.webticker.min.js') }}"></script>
    <meta charset="UTF-8">
    <meta name="description" content="Interfaz">
    <meta name="keywords" content="New Order">
    <meta name="author" content="Shahraz,Toni,Rafa">
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/maincss.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Bungee&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>


<body id="main">
    <header></header>
    <div class="container">
        <nav class="page__menu page__custom-settings menu">
            <ul class="menu__list r-list">
                <div class="logo">
                    <p>N|E</p>
                </div>
                <li class="menu__group"><a href="{{ asset('/') }}" class="menu__link r-link text-underlined">Inici </a></li><li>
                <li class="menu__group"><a href="{{ asset('/pagina2') }}" class="menu__link r-link text-underlined">Tienda </a></li>
                <li class="menu__group"><a href="{{ asset('/pagina4') }}" class="menu__link r-link text-underlined">Parametrizable</a></li>
            </ul>
        </nav>
    <section id="Home">
        <h1 id="NewOrder" class="animate__animated animate__zoomInDown animate__slow">New Order</h1>
        <p id="autores">Per: Shahraz Ahmad , Toni Lupiañez y Rafa Gonzalez</p>
    </section>
    <section id="About">
        <h1 id="New Order">About</h1>
        <p>Benvinguts a New Order Portal per Pura Calle.
            Durant el periode de 2049-2055 ha esdevingut una brutal sacsejada social.Ara amb l'arribada de una nova ordre mundial els integrants de Pura Calle em creat aquesta web per facilitar les transaccions i la comunicacio al món. </p>
    </section>
    <footer>
        <ul id="webticker" >
            <li data-update="item1">barrio comePeos: esta madrugada se ha producido una explosion bioQuimica en las inmediaciones del centro de recursos organicos del barrio comePeos. de las 10 victimas del atentado solo una ha sido de caracter mortal pero toda la cosecha de esta temporada se ha visto afectada...
.</li>
            <li data-update="item2">Distrito cagaSagua: las victimas del maremoto del distrito cagaSagua siguen en aumento. 1438 victimas mortales, 4873 victimas graves y 7392 leves...
</li>
</ul>
    <h2>&copy;Copyright per Shahraz,Toni i Rafa<h2>

    </footer>
    </div>

</body>

</html>

@section('page-js-script')
<script type="text/javascript">
    AOS.init();
$("#webticker").webTicker({
    height:'75px',
    duplicate:true,
    rssfrequency:0,
    startEmpty:false,
    hoverpause:false,
    transition: "ease"
});

</script>

