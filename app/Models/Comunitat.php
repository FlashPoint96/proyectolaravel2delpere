<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comunitat extends Model
{
    protected $table = 'Comunitat';
    protected $primaryKey = 'id';
    protected $fillable = ['Comunitat','idComunitat'];

    public function user()
    {
        return $this->hasMany(user::class);
    }

}
